# Desenvolvimento Web 1 - UNIME

Este repositório contém as atividades da disciplina de Desenvolvimento Web 1, ministrada pelo Professor Paulo Reis no curso de BSI da UNIME.

## Sumário
- [Introdução](#introdução)
- [Atividades](#atividades)
- [Contribuidores](#contribuidores)

## Introdução
Neste repositório, você encontrará soluções para diversos exercícios e projetos de desenvolvimento web. A disciplina aborda tópicos como HTML, CSS, JavaScript e muito mais.

## Atividades
- Atividade 1: Prêmio - Introdução ao HTML.
- [Codigo Fonte](https://github.com/MichelNsouza/Web1Unime/tree/main/Atividade1) 
- [Git Pages](https://michelnsouza.github.io/Web1Unime/Atividade1/index.html)

- Atividade 2: Minhas Tarefas - Introdução ao CSS.
- [Codigo Fonte](https://github.com/MichelNsouza/Web1Unime/blob/main/Atividade2) 
- [Git Pages](https://michelnsouza.github.io/Web1Unime/Atividade2/index.html)

- Atividade em Sala de Aula: CertifiqueMe.
- [Codigo Fonte](https://github.com/MichelNsouza/Web1Unime/blob/main/AtividadeSaladeAula/Atividade1/index.html) 
- [Git Pages](https://michelnsouza.github.io/Web1Unime/AtividadeSaladeAula/Atividade1/index.html)

## Contribuidores
- Professor Paulo Reis
- Alunos do curso de BSI da UNIME
